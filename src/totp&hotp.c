/*
 ============================================================================
 Name        : totp&hotp.c
 Author      : 
 Version     :
 Copyright   : Oui
 Description : POC de HOTP et TOTP
 ============================================================================
 */

#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>
#include<string.h>
#include<wolfssl/ssl.h>
#include<time.h>

static const int PuissanceDe10[] = {10,100,1000,10000,100000,1000000,10000000,100000000,1000000000,1000000000};


void otp(const byte* key,size_t taille,uint64_t facteurChangeant, int NbChiffre)
{
    byte hash[SHA_DIGEST_SIZE];
    byte sel[8];
    int offset;
    int binaire;
    int i;
    for (i = sizeof(sel) - 1; i >= 0; i--) {
        sel[i] = (byte)(facteurChangeant & 0xff);
        facteurChangeant >>= 8;
    }
    Hmac hmac;
    if(wc_HmacSetKey(&hmac,SHA,key, taille)==0){
    	//printf("Set key passé \n");
    }
    if(wc_HmacUpdate(&hmac, sel, sizeof(sel))==0){
    	//printf("Update passé \n");
    }
    if(wc_HmacFinal(&hmac, hash)==0){
    	//printf("Final passé \n");
    }

    offset = hash[sizeof(hash) - 1] & 0x0f;

    binaire = ((hash[offset] & 0x7f) << 24) | ((hash[offset + 1] & 0xff) << 16)
        | ((hash[offset + 2] & 0xff) << 8) | (hash[offset + 3] & 0xff);

    int otp = binaire % PuissanceDe10[NbChiffre-1];
    if((NbChiffre-2)>=0)
    {
        while (otp < PuissanceDe10[NbChiffre-2] && otp!=0) {
             otp=otp*10;
         }
    }
    printf("%d \n",otp);
}

int calculDeLaValeurTemps(time_t tempsActuel){
	int temps=tempsActuel/3600; //1 heure
	return temps;
}

int main()
{
	time_t tempsActuel;
	time(&tempsActuel);
	printf("%ld \n",tempsActuel);
	int essais=0;
	//printf("nb essais : ");
	//scanf("%d",&essais);
	const byte key[]={'1','2','3','4','5','6','7','8','9','0','1','2','3','4','5','6','7','8','9','0'};
	int temps=calculDeLaValeurTemps(tempsActuel);
    //le for commenté doit être utilisé pour le HOTP et l'iteration doit être passée en paramètre à la place du temps
    //Dans le cas d'une implémentation réelle le nombre se doit de ne jamais reset pour ne pas répéter les codes
	//for(int iteration=0;iteration<essais;iteration++)
	//{
		totp(key,sizeof(key),temps,6); //1->la clé|2->la taille de la clé|3->la valeur changeante|4->la taille du code de retour
	//}
	return 0;
}
